//console.log("Hellow World!");


//JS DOM - Javascript Document Object Model

// For CSS all elements were consider as a box.

// This is our css box model. 

//Javascript, all html elements is considered as an object. this is what we 
//call JS Document Object Model.
//document is a keyword in js which refers to the whole document/html page.

//
console.log(document);

//store/select 



let firstNameLabel = document.querySelector("#label-first-name");

console.log(firstNameLabel); 

let lastNameLabel = document.querySelector("#label-last-name");
console.log(lastNameLabel);




console.log(firstNameLabel.innerHTML);


firstNameLabel.innerHTML = "I love New York City.";


lastNameLabel.innerHTML = "My Favorite Food is Fried Chicken."


let city = "Tokyo";

if(city ===  "New York"){

	firstNameLabel.innerHTML = `I like New York City.`
} else {
	firstNameLabel.innerHTML = `I don't like New York. I like ${city} city.`
}


firstNameLabel.addEventListener('click',()=>{

	firstNameLabel.innerHTML = "I've been clicked. Send help!"

	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh"

})

	
lastNameLabel.addEventListener('click',()=>{

//	lastNameLabel.style.color = "green";
//	lastNameLabel.style.fontSize = "5vh"

	if(lastNameLabel.style.color === "green"){
		lastNameLabel.style.color = "black";
		lastNameLabel.style.fontSize = "16px";
	} else {
		lastNameLabel.style.color = "green";
		lastNameLabel.style.fontSize = "5vh";
	}

})

let inputFirstName = document.querySelector("#txt-first-name");

//.value is aproperty of input elements. It contains the current
//value of the element.


console.log(inputFirstName.value);

/*inputFirstName.addEventListener('keyup',()=>{

	console.log(inputFirstName.value);
})*/


let fullNameDisplay = document.querySelector("#full-name-display");

console.log(fullNameDisplay)

let inputLastName =  document.querySelector("#txt-last-name");

console.log(inputLastName)


/*inputLastName.addEventListener('keyup',()=>{

	console.log(inputLastName.value)
})*/



//Syntax
//element.addEventListener(<event>,<function>);

const showName = () => {

	console.log(inputFirstName.value);
	console.log(inputLastName.value);
	//console.log(fullNameDisplay.innerHTML);//results to blank because
	//the h3 doesnt have a text content or children.

	fullNameDisplay.innerHTML = inputFirstName.value+" "+inputLastName.value;

}

inputFirstName.addEventListener('keyup',showName);


inputLastName.addEventListener('keyup',showName);






































